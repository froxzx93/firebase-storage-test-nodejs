import fs from 'fs';
import path from 'path';

const createFile = async () => {
    console.log("upload success")
    fs.writeFileSync(path.join(__dirname, './', "testUpload.txt"), 'Hey there!');
}

export { createFile };