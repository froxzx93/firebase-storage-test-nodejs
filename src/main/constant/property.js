import file_key from '../../../firebase_key.json';
import path from 'path';

const port = 3000;
const bucket_name = "gs://storage";
const private_key = file_key;
const foler_name_download = path.join(__dirname, '../../', "resources/download/");
const foler_name_upload = path.join(__dirname, '../../', "resources/upload/");
export { bucket_name, private_key, foler_name_download, port, foler_name_upload };