import { Router } from 'express';
import { listFiles, downloadFile, uploadFile } from '../firebase/methods';

const router = Router();

router.get('/', function (req, res) {
    res.send('Hello World')
})

router.get('/listFiles', async (req, res) => {
    const response = await listFiles()
    res.send(response)
})

router.get('/downloadFile', async (req, res) => {
    const response = await downloadFile(req.query.pathFile)
    res.send(response)
})

router.post('/uploadFile', async (req, res) => {
    const response = await uploadFile(req.body.fileName, req.body.pathStorage)
    res.send(response)
})

export { router };