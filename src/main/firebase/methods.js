import { bucket } from './client';
import path from 'path';
import { foler_name_download, foler_name_upload } from '../constant/property';

const response = {
    status: 200,
    data: {}
};

const listFiles = async () => {
    response.status = 200;
    try {
        const list = [];
        const [files] = await bucket.getFiles();
        files.forEach(file => {
            list.push(file.name)
        });
        response.data = list
    } catch (err) {
        response.status = 500;
        response.data = err;
    }
    return response;
};

const downloadFile = async (pathFile) => {
    response.status = 200;
    try {
        const fileName = getFileName(pathFile)
        const downloadOptions = {
            destination: path.join(foler_name_download, fileName)
        };
        await bucket.file(pathFile).download(downloadOptions);
        response.data = "download success";
    } catch (err) {
        response.status = 500;
        response.data = err;
    }
    return response;
}


const uploadFile = async (fileName, pathStorage) => {
    response.status = 200;
    try {
        const file = path.join(foler_name_upload, fileName);
        const destinationFilename = pathStorage + fileName;
        bucket.upload(file, { destination: destinationFilename });
        response.data = "upload success";
    } catch (err) {
        response.status = 500;
        response.data = err;
    }
    return response;
};

const getFileName = (string) => {
    const texts = string.split("/");
    return texts[texts.length - 1];
};

export { listFiles, downloadFile, uploadFile }