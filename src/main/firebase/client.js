import admin from 'firebase-admin';
import { bucket_name, private_key } from '../constant/property';

admin.initializeApp({
    credential: admin.credential.cert(private_key),
});

const bucket = admin.storage().bucket(bucket_name);
export { bucket }