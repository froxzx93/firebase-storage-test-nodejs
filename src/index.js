import express from 'express';
import { port } from './main/constant/property';
import { createFile } from './resources/upload/index';
import { router } from './main/router/restController';

/**
 * Crear archivo de prueba
 */
createFile();

const app = express()

app.use(express.json());
app.use("/v1", router);
app.listen(port)